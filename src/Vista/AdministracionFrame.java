/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Utilidades.Utilidades;
import javax.swing.JPanel;

/**
 *
 * @author Nana JG
 */
public class AdministracionFrame extends javax.swing.JFrame {
    
    private JPanel panel;
    private GestionarPersona gestionarPersona;
    private GestionarProveedor gestionarProveedor;
    private GestionarProducto gestionarProducto;
    private GestionarUsuario gestionarUsuario;
    


    public AdministracionFrame() {       
        initComponents();
        Utilidades.formatearFrame(this, contenidoPanel, jMenuBar1);
        gestionarPersona = new GestionarPersona();
        gestionarProveedor = new GestionarProveedor();
        gestionarProducto = new GestionarProducto();
        gestionarUsuario =  new GestionarUsuario();
        
    }
    
    public void cambiarPanel(JPanel panel1) {
        contenidoPanel.remove(labelFondo);
        if (panel != null) {
            contenidoPanel.remove(panel);
        }
        panel = panel1;
        panel.setBounds(0, 0, Utilidades.ANCHO_PANEL_PREDETERMINADO, Utilidades.ALTO_PANEL_PREDETERMINADO);
        contenidoPanel.add(panel);
        panel.updateUI();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenu1 = new javax.swing.JMenu();
        contenidoPanel = new javax.swing.JPanel();
        labelFondo = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();

        jMenu1.setText("jMenu1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        contenidoPanel.setBackground(new java.awt.Color(255, 255, 255));
        contenidoPanel.setPreferredSize(new java.awt.Dimension(1470, 810));
        contenidoPanel.setLayout(null);

        labelFondo.setText("Bienvenido");
        contenidoPanel.add(labelFondo);
        labelFondo.setBounds(50, 50, 210, 70);

        getContentPane().add(contenidoPanel);
        contenidoPanel.setBounds(0, 0, 1430, 660);

        jMenu2.setText("Administración");
        jMenu2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jMenuItem3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jMenuItem3.setText("Gestionar personas");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        jMenuItem1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jMenuItem1.setText("Gestionar proveedor");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem1);

        jMenuItem2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jMenuItem2.setText("Gestionar Productos");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem2);

        jMenuItem4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jMenuItem4.setText("Gestionar Usuarios");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem4);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        cambiarPanel(gestionarPersona);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
       cambiarPanel(gestionarProveedor);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        cambiarPanel(gestionarProducto);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        cambiarPanel(gestionarUsuario);
    }//GEN-LAST:event_jMenuItem4ActionPerformed



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel contenidoPanel;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JLabel labelFondo;
    // End of variables declaration//GEN-END:variables
}
