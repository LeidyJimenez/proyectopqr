package Utilidades;

import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;


public class Utilidades {

    /**
     * Ancho del frame ppal.
     */
    public static final int ANCHO_PREDETERMINADO = 1300 - 45;

    /**
     * Alto del frame ppal.
     */
    public static final int ALTO_PREDETERMINADO = 740 - 45;

    /**
     * Ancho del panel actual.
     */
    public static final int ANCHO_PANEL_PREDETERMINADO = 1250;

    /**
     * Alto del panel actual.
     */
    public static final int ALTO_PANEL_PREDETERMINADO = 580;

    /**
     * Ancho del frame ppal en pcs mini .
     */
    public static final int ANCHO_MINI = 1000;

    /**
     * Alto del frame ppal en pcs mini
     */
    public static final int ALTO_MINI = 550;

    /**
     * Ancho del panel actual en pcs mini.
     */
    public static final int ANCHO_PANEL_MINI = 950;

    /**
     * Alto del panel actual en pcs mini.
     */
    public static final int ALTO_PANEL_MINI = 440;

    /**
     * El frame ppal del usuario.
     */
    public static JFrame frameUsuario = new JFrame();
  

    /**
     * Formatea el frame con tamaños, titulos, iconos y menus.
     *
     * @param frame Frame
     * @param contenidoPanel Contenido panel
     * @param menu Menu
     */
    public static void formatearFrame(JFrame frame, JPanel contenidoPanel, JMenuBar menu) {
        //Ubicacion
        frame.setLayout(null);
        frame.setTitle("PQR Service");
        //Icono
//        frame.setIconImage(new ImageIcon(frame.getClass().getResource("/Recursos/icon.png")).getImage());
        frameUsuario = frame;
        //Tamaño por defecto
        frame.setSize(ANCHO_PREDETERMINADO, ALTO_PREDETERMINADO);
        //frame.getRootPane().setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.RED));
        //Se obtiene le tamaño de la pantalla del pc
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth();
        double height = screenSize.getHeight();

        //Si la resolucion de la pantalla es menor que el tamaño predeterminado
        if (width < ANCHO_PREDETERMINADO || height < ALTO_PREDETERMINADO) {
            frame.getContentPane().remove(contenidoPanel);

            //Se agregan scrolls
            JScrollPane jScrollPane1 = new JScrollPane(
                    ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                    ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
            jScrollPane1.getVerticalScrollBar().setUnitIncrement(15);
            frame.getContentPane().add(jScrollPane1);
            jScrollPane1.setLocation(0, 50);
            //Establece tamaño
            jScrollPane1.setSize(ANCHO_MINI - 5, ALTO_PANEL_MINI);
            jScrollPane1.add(contenidoPanel);
            contenidoPanel.setPreferredSize(new Dimension(ANCHO_PREDETERMINADO, ALTO_PREDETERMINADO));
            jScrollPane1.setViewportView(contenidoPanel);
            frame.setSize(ANCHO_MINI, ALTO_MINI);
        }
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
    }


    /**
     * Cierra la sesion y permite loguearse como otro usuairo.
     *
     * @param frame Frame
     */
//    public static void cerrarSesion(JFrame frame) {
//        FramePpal frameN = new FramePpal();
//        frameN.verInicio();
//        frameN.setVisible(true);
//        frame.dispose();
//    }



    /*Method
    * Metodo que centra los encabezados y registros de
    * todas las columnas de una tabla.
    * @param table // Recibe la tabla a la que se le desea dar el formato
    * By (Kmilo M)
     */
    public static void formatTable(JTable table) {
        DefaultTableCellRenderer modeloCentrar = (DefaultTableCellRenderer) table.getTableHeader().getDefaultRenderer();
        modeloCentrar.setHorizontalAlignment(SwingConstants.CENTER);
        modeloCentrar = new DefaultTableCellRenderer();
        modeloCentrar.setHorizontalAlignment(SwingConstants.CENTER);
        for (int i = 0; i < table.getColumnCount(); i++) {
            table.getColumnModel().getColumn(i).setCellRenderer(modeloCentrar);
        }
    }

}
