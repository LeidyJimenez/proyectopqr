/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Nana JG
 */
public class Conexion {
    
    public static Connection getConexion(){
        String url = "jdbc:sqlserver://localhost:1433;databaseName=dbpqrservice";
        String usuario = "jasservice";
        String clave = "jasservice";
        String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        return getConexion(url, usuario, clave, driver);
    }
       public static Connection getConexion(String url, String usuario,
            String clave, String driver) {

        Connection con = null;
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(url, usuario, clave);
        } catch (ClassNotFoundException | SQLException ex) {
        }
        return con;
    }
    
    public void Cerrar() throws Exception {

        Connection con = null;
        try {
            if (con != null) {
                if (con.isClosed() == false) {
                    con.close();
                }
            }
        } catch (SQLException e) {
            throw e;
        }

    }

}
