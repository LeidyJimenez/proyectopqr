package Utilidades;

import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

/**
 * Contiene metodos para mosrar mensajes al usuairo .
 *
 * @author Santiago Román
 * @version 6/10/2018
 */
public class Mensajes {

    public static Object mostrarMensajeComboBox(ArrayList objetos, String titulo) {
        return JOptionPane.showInputDialog(null, titulo, "Selección de opción",
                JOptionPane.INFORMATION_MESSAGE, null, objetos.toArray(), objetos.get(0));
    }

    public static <T> ArrayList<T> mostrarMensajeList(ArrayList<T> objetos, String titulo) {
        JList list = new JList();
        JScrollPane jscrollpane = new JScrollPane();
        jscrollpane.setViewportView(list);

        DefaultListModel listModel = new DefaultListModel();

        for (T objeto : objetos) {
            listModel.addElement(objeto);
        }
        list.setModel(listModel);
        JOptionPane.showMessageDialog(null, jscrollpane, titulo + "\n\nSeleccione las opciones", JOptionPane.PLAIN_MESSAGE);
        ArrayList<T> seleccionados = new ArrayList<T>(list.getSelectedValuesList());
        return seleccionados;
    }

    /**
     * Muestra un mensaje de error al usuario
     *
     * @param cadena Texto a mostrar
     */
    public static void mostrarMensajeError(String cadena) {
        JOptionPane.showMessageDialog(null, cadena,
                "Error", JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Muestra un mensaje de informacion al usuario
     *
     * @param cadena Texto a mostrar
     */
    public static void mostrarMensajeInformacion(String cadena) {
        JOptionPane.showMessageDialog(null, cadena,
                "Informacion", JOptionPane.INFORMATION_MESSAGE);
    }

    public static void mostrarMensajeAdvertencia(String cadena) {
        JOptionPane.showMessageDialog(null, cadena,
                "Advertencia", JOptionPane.WARNING_MESSAGE);
    }

    /**
     * Muestra un mensaje que pide al usuario seleccionar SI o NO
     *
     * @param cadena Mensaje a mostrar
     * @return <tt>true</tt> si el usuario dio SI, <tt>false</tt> si dio NO
     */
    public static boolean mostrarMensajeConfirmacion(String cadena) {
        //devuelve true si se dio Si, o false si se dio NO
        int op = JOptionPane.showConfirmDialog(null, cadena,
                "Aviso", JOptionPane.YES_NO_OPTION);
        if (op == JOptionPane.YES_OPTION) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Muestra un InputDialog con el mensaje recibido
     *
     * @param cadena Texto a mostrar
     * @return <tt>String</tt> Cadena con el mensaje escrito por el usuario,
     * <tt>null</tt> si dio cancelar, <tt>""</tt> si no se ingresó nada
     */
    public static String mostrarMensajeIngreso(String cadena, String valorInicial) {
        //retorna la cadena ingresada en el inputdialog
        return JOptionPane.showInputDialog(cadena, valorInicial);
    }

    public static String mostrarMensajeIngreso(String cadena) {
        return mostrarMensajeIngreso(cadena, "");
    }
}
